import React, { Component } from 'react';
import './App.css';

import Timeline  from "./components/Timeline";

class App extends Component {
  render() {
    return (
      <div className="App container-fluid">
        <div className='d-flex flex-row flex-wrap flex-md-nowrap justify-content-around'> 
          <Timeline className=''/>
        </div>
        
      </div>
    );
  }
}

export default App;
