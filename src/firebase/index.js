import db from './initFirebase'

export default {

    getTweets(){
        console.log('Database', db)
        const homeTweetsCollection = db.collection('homeTweets');
        return homeTweetsCollection.get().then((querySnapshot) => {
            console.log('Gotten hometimeline tweets.');
            const tweets = querySnapshot.docs.map(doc => {
                return doc.data()
            })
            return tweets;

        });
    }
}


