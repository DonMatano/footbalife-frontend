var firebase = require('firebase/app');
require('firebase/firestore');
const firebaseConfig = require('../config/firebase');

const firebaseApp = firebase.initializeApp({
    apiKey: firebaseConfig.credential.apiKey,
    authDomain: firebaseConfig.credential.authDomain,
    projectId: firebaseConfig.credential.projectId
})

const settings = { /* your settings... */
    timestampsInSnapshots: true
};

const firestore = firebaseApp.firestore();
firestore.settings(settings)

export default firestore;