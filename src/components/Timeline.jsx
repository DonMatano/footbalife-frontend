import React, { Component } from 'react';
import { Card, CardHeader } from 'reactstrap';
import '../styles/Timeline.css';
import firebase from '../firebase/index'

import Tweet from './Tweet';

class Timeline extends Component {

    constructor(props){
        super(props);
        this.state = {
            tweets: []
        }
    }

    render() {
        return (
            <div>
                {/* <h1 className="timeline">Timeline</h1> */}
                <Card>
                    <CardHeader>Timeline</CardHeader>
                    <Tweet className='mb-1' tweets={this.state.tweets} />
                </Card>
            </div>
        );
    }

    componentDidMount() {
        firebase.getTweets().then(gottenTweets => {
            console.log('Gotten tweets', gottenTweets)
            this.setState(prevState => {
                return {
                    tweets: [...prevState.tweets, ...gottenTweets]
                }
            });
        }).catch(err => {
            console.log("Error ", err)
        })
    }

}

export default Timeline;