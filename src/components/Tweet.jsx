import React, { Component } from 'react';
import {Card} from 'reactstrap';
import FontAwesome from '@fortawesome/react-fontawesome'
import {faComment, faHeart, faEnvelope} from '@fortawesome/fontawesome-free-regular'
import { faRetweet } from '@fortawesome/fontawesome-free-solid'
import '../styles/Tweet.css';

class Tweet extends Component {
    render() {

        let tweetTimeline = this.props.tweets.map((tweet, index) => {
            return(
                // <div className="tweet" key={index}>
                //     <img src={tweet.user.profile_image_url_https} alt={`Profile pic of ${tweet.user.screen_name}`}/>
                //     <p className="tweet_text">{tweet.full_text}</p>
                // </div>
                <div key={index}>
                    

                    <Card body>
                        <div>
                            <div className='d-flex flex-row'>
                                <img className='mr-2 rounded-circle d-inline' src={tweet.user.profile_image_url_https} alt={`Profile pic of ${tweet.user.screen_name}`}/>
                                <div className='d-flex flex-column'>
                                    <span className='user-name'>{tweet.user.name}</span>
                                    <span className='user-screen-name'>@{tweet.user.screen_name}</span>
                                </div>
                            </div>
                            
                            <div className='tweet-text my-2'>
                                <p className=''>{tweet.full_text}</p>
                            </div>

                            <div className='d-flex flex-row'>
                                <FontAwesome className='mx-2' icon={faComment}></FontAwesome>
                                <FontAwesome className='mx-2' icon={faRetweet}></FontAwesome>
                                <FontAwesome className='mx-2' icon={faHeart}></FontAwesome>
                                <FontAwesome className='mx-2' icon={faEnvelope}></FontAwesome>
                            </div>
                            
                        </div>
                    </Card>
                    
					{/* <div className="media">
						<a className="media-left" href="#">
                            <img alt={`Profile pic of ${tweet.user.screen_name}`} className="media-object img-rounded" src={tweet.user.profile_image_url_https}/>
                        </a>
						<div className="media-body">
							<h4 className="media-heading">{tweet.user.screen_name}</h4>
							<p>{tweet.full_text}</p>
							<ul className="nav nav-pills nav-pills-custom">
								<li><a href="#"><span className="glyphicon glyphicon-share-alt"></span></a></li>
								<li><a href="#"><span className="glyphicon glyphicon-retweet"></span></a></li>
								<li><a href="#"><span className="glyphicon glyphicon-star"></span></a></li>
								<li><a href="#"><span className="glyphicon glyphicon-option-horizontal"></span></a></li>
							</ul>
						</div>
					</div> */}
                </div>
            )
        });

        return (
            tweetTimeline
        );
    }
}

export default Tweet;